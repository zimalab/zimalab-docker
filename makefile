BUILD_IMG="docker image build"
IMAGE="timofeyn/audiowaveform:alpine3.13"
DIR="tools/audiowaveform"

all:
	make awf
	make php80
	make php81

awf:
	make build IMAGE="timofeyn/audiowaveform:alpine3.14" TARGET="audiowaveform" DIR="tools/audiowaveform"

php80:
	make build IMAGE="timofeyn/zimalab-php:8.0-alpine3.14" TARGET="php" DIR="php/8.0"

php81:
	make build IMAGE="timofeyn/zimalab-php:8.1-alpine3.17" TARGET="php" DIR="php/8.1"
	make build IMAGE="timofeyn/zimalab-php:8.1-alpine3.17-dev" TARGET="php-dev" DIR="php/8.1"

build:
	cd $(DIR) && docker build --pull -t "$(IMAGE)" --target "$(TARGET)" . && docker push "$(IMAGE)"
