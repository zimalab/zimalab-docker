ARG PHP_VERSION=8.0
ARG ALPINE_VERSION=3.14

FROM php:${PHP_VERSION}-fpm-alpine${ALPINE_VERSION} AS php

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# persistent / runtime deps and most useful php extensions
RUN apk add --no-cache \
		acl \
		file \
		gettext \
		git \
		mariadb-client mariadb-common mariadb-connector-c \
		supervisor \
		logrotate \
		fcgi && \
    chmod +x /usr/local/bin/install-php-extensions && \
    sync && \
    install-php-extensions opcache mysqli pdo_mysql pdo_pgsql intl apcu imagick sockets amqp zip exif gd mongodb pcntl

COPY --from="composer:2" /usr/bin/composer /usr/bin/composer

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="${PATH}:/root/.composer/vendor/bin"
