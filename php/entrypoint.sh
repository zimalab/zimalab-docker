#!/bin/sh

set -e

env

if [[ -z "${APP_ENV}" ]]; then
  echo "APP_ENV variable not specified"
  exit 1
fi

if [ "${APP_ENV}" != "prod" ]; then
  composer install --prefer-dist --no-progress --no-interaction
  php app/console assets:install --symlink
else
  composer install --prefer-dist --no-progress --no-interaction --optimize-autoloader --classmap-authoritative --no-dev
		bin/console assets:install --no-interaction
fi

until bin/console doctrine:query:sql "select 1" >/dev/null 2>&1; do
    (>&2 echo "Waiting for MySQL to be ready...")
  sleep 2
done

if [ "$(ls -A src/Migrations/*.php 2> /dev/null)" ]; then
  bin/console doctrine:migrations:migrate --no-interaction
fi

#if [[ -z "${APP_ENV}" ]]; then
#  echo "APP_ENV variable not specified"
#  exit 1
#fi

exec docker-php-entrypoint php-fpm