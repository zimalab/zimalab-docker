#!/bin/sh
set -e

if [ "$(ls -A /etc/container_init/)" ]; then
  for f in /etc/container_init/*; do
    chmod +x "$f"
    sh "$f"
  done
else
  echo "Init scripts directory /etc/container_init/ is empty"
fi


# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
        set -- php-fpm "$@"
fi

exec "$@"
